import { Component, OnInit } from '@angular/core';
import {DataService} from '../data.service';
import {Fixtures} from '../Fixtures';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  fixtures : Fixtures[];

  constructor(private dataService:DataService) { }

  ngOnInit() {
      this.loadTeam();
  }


 
  loadTeam() {
   
    this.dataService.getTeam('73').subscribe(fixtures => {
    this.fixtures = fixtures;
    console.log(this.fixtures);
    });
  
   }
}
