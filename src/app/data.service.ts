import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import {Standing} from './Standing';
import {Fixtures} from './Fixtures';

const httpOptionsA = {
	headers: new HttpHeaders ({
		'X-Auth-Token': 'ccf7c783ee20446aa1e56dc6d01b542c'
	})
	};


@Injectable()
export class DataService {

	

  constructor(private http: HttpClient) {
  	 
   }


  getTable (leagueID : string) : Observable <Standing[]>
  { // pass in the league ID


  		let urlPrefix = "http://api.football-data.org/v1/competitions/";
		let urlSuffix = "/leagueTable";
		
		return this.http.get<Standing[]>(urlPrefix+leagueID+urlSuffix, httpOptionsA); 
		
  		
  }

  getTeam (teamID : string) : Observable <Fixtures[]>
  { // pass in the league ID


  		let urlPrefix = "http://api.football-data.org/v1/teams/";
		let urlSuffix = "/fixtures";
		
		return this.http.get<Fixtures[]>(urlPrefix+teamID+urlSuffix, httpOptionsA); 
		
  		
  }

}
