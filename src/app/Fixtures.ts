import {Home} from './home';

export class Fixtures {
	constructor(public _links: string, public date: string, public status: string, public matchday: string, public homeTeamName: string, public awayTeamName: string,public result: {goalsHomeTeam: number,goalsAwayTeam: number}) {} 
}

