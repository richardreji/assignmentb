import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';
import { NewsComponent } from './news/news.component';
import{RouterModule, Routes} from '@angular/router';
import { NavbarComponent } from './navbar/navbar.component';
import { ParallaxHeadComponent } from './parallax-head/parallax-head.component';
import { LadderComponent } from './ladder/ladder.component';
import {DataService} from './data.service';
import { ResultsParentComponent } from './results-parent/results-parent.component';
import { ResultsChildComponent } from './results-child/results-child.component';
import { RmrComponent } from './rmr/rmr.component';

@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    NewsComponent,
    NavbarComponent,
    ParallaxHeadComponent,
    LadderComponent,
    ResultsParentComponent,
    ResultsChildComponent,
    RmrComponent
  ],
  imports: [
    BrowserModule, HttpClientModule,
    RouterModule.forRoot([
      {path: 'about', component:AboutComponent},
      {path: 'news', component:NewsComponent},
      //{path: 'parallax', component:ParallaxHeadComponent}
      

    ])
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
